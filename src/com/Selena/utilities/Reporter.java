package com.Selena.utilities;

import api.utils.Formatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Reporter {
	public static void log(String s) {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("H:mm:ss:SSS");
	    sdf.format(cal.getTime());
		org.testng.Reporter.log("[" + sdf.format(cal.getTime()) + "]: " + Formatter.escapeCharacters(s) + " <br></br>");
	}

}
