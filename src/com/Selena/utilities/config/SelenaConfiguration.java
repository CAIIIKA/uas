package com.Selena.utilities.config;

import com.Selena.Configuration;



/**
 * Provides access to system properties.
 *
 * @author meza <meza@meza.hu>
 *
 */
public final class SelenaConfiguration extends Configuration
{

}

