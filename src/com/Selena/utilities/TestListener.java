package com.Selena.utilities;

import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.*;


public class TestListener extends TestListenerAdapter {
	@Override
	public void onTestFailure(ITestResult result) {
		File file = new File("");
        String imageName = result.getName() + "-" + System.currentTimeMillis() + ".png";
		org.testng.Reporter.setCurrentTestResult(result);
		Reporter.log(" screenshot saved  <br></br> <a href = '" + imageName + "'><img src = '" + imageName + "' hight='800' width='600'/> </a>");
		
		File scrFile = ((TakesScreenshot)BaseTest.driver).getScreenshotAs(OutputType.FILE);

        try {
			FileUtils.copyFile(scrFile, new File(file.getAbsolutePath()+ File.separatorChar + "html" + File.separatorChar + imageName));
		} catch (IOException e) {
			Reporter.log("Error saving screenshot!");
		}

        File counterFile = new File(System.getProperty("output.dir") + File.separator + "fails");

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(counterFile));
            String str = bufferedReader.readLine();
            int counter = 0;
            if(str != null) {
                counter = Integer.parseInt(str);
            }
            counter++;
            bufferedReader.close();

            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(counterFile, false));
            bufferedWriter.write(String.valueOf(counter));
            bufferedWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
	
	
}
