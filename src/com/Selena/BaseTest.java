package com.Selena;

import api.Actions;
import api.Pages;
import api.utils.CustomRemoteWebDriver;
import com.Selena.utilities.Reporter;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.*;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * 
 * The Base of all tests.
 * 
 * @author Brautigam Gergely
 * 
 */
public class BaseTest {



    public static void execute(String batch, String ... params) throws IOException {
        String[] startLine = ArrayUtils.addAll(new String[]{batch}, params);
        Process p = Runtime.getRuntime().exec(startLine);
        BufferedReader std = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = std.readLine()) != null) {
            System.out.println(line);
        }
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        std.close();
    }
	/**
	 * Configuration.
	 */
	public static WebDriver driver;

	protected static int defaultElementTimeout = 30;


	protected void setupFirefoxDriver() throws IOException {
		driver = new FirefoxDriver();
        driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
    }

    protected void setupFirefoxRemoteDriver(String hubUrl, String platformName) throws IOException {
        Platform platform =  (platformName != null) ? Platform.valueOf(platformName) : Platform.ANY;

        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setBrowserName("firefox");
        capabilities.setPlatform(platform);
        capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);

        driver = new CustomRemoteWebDriver(new URL(hubUrl), capabilities);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
    }
	
	protected void setupFirefoxDriver(String profileLocation){
		FirefoxProfile ffProfile = new FirefoxProfile(new File(profileLocation));
		driver = new FirefoxDriver(ffProfile);
		driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
	}

    protected void setupIEDriver() throws IOException {
        String pathToDriver = System.getProperties().getProperty("browsers.dir") + File.separatorChar + "IEDriverServer_32.exe";
        System.setProperty("webdriver.ie.driver", pathToDriver);
        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setCapability("nativeEvents", false);
        driver = new InternetExplorerDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
    }

    protected void setupIERemoteDriver(String hubUrl, String platformName) throws IOException {
        Platform platform =  (platformName != null) ? Platform.valueOf(platformName) : Platform.ANY;

        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setBrowserName("internet explorer");
        capabilities.setPlatform(platform);
        capabilities.setCapability("nativeEvents", false);
        capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);

        driver = new CustomRemoteWebDriver(new URL(hubUrl), capabilities);
        driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
    }

    protected void setupChromeDriver() throws IOException {
        String platform = System.getProperty("platform");

        String driversFolder = System.getProperties().getProperty("browsers.dir") + File.separatorChar;
        String pathToDriver = (platform.equals("WINDOWS")) ? driversFolder + "chromedriver.exe" : driversFolder + "chromedriver";

        System.setProperty("webdriver.chrome.driver", pathToDriver);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        String userProfile= System.getProperty("profile");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);


        driver = new ChromeDriver(capabilities);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
    }

    protected void setupChromeRemoteDriver(String hubUrl, String platformName) throws IOException {
        Platform platform =  (platformName != null) ? Platform.valueOf(platformName) : Platform.ANY;

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setBrowserName("chrome");
        capabilities.setPlatform(platform);
        capabilities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);

        driver = new CustomRemoteWebDriver(new URL(hubUrl), capabilities);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(defaultElementTimeout, TimeUnit.SECONDS);
    }


	

	protected void stopDriver(){
		driver.quit();
	}

	/**
	 * An function that fetches the table from the excel sheet and returns it as
	 * an array of array of String type. It uses Java Excel API to fetch data
	 * from excel sheet.
	 * 
	 * @param xlFilePath
	 *            the path of XL file/workbook containing the data, the path is
	 *            relative to java qatestlab
	 * @param sheetName
	 *            name of the xls sheet that contains the table
	 * @param tableName
	 *            Name of the table that you wish to fetch
	 */
	public String[][] getTableArray(String xlFilePath, String sheetName,
			String tableName) throws Exception {
		String[][] tabArray = null;

		Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
		Sheet sheet = workbook.getSheet(sheetName);
		int startRow, startCol, endRow, endCol, ci, cj;
		Cell tableStart = sheet.findCell(tableName);
		startRow = tableStart.getRow();
		startCol = tableStart.getColumn();

		Cell tableEnd = sheet.findCell(tableName, startCol + 1, startRow + 1,
				100, 64000, false);

		endRow = tableEnd.getRow();
		endCol = tableEnd.getColumn();

		tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
		ci = 0;

		for (int i = startRow + 1; i < endRow; i++, ci++) {
			cj = 0;
			for (int j = startCol + 1; j < endCol; j++, cj++) {
				tabArray[ci][cj] = sheet.getCell(j, i).getContents();
			}
		}
		return (tabArray);
	}


    @BeforeClass
    public void setUp() throws IOException, AWTException {
        String message = "* Starting test " + this.getClass().toString();
        Reporter.log("\n" + message);
        System.out.println(message);


        String hubUrl = System.getProperty("hub");
        String browser = (System.getProperty("browser") == null) ? "firefox" : System.getProperty("browser");
        String platform = System.getProperty("platform");

        if(browser.equalsIgnoreCase("ie")) {
            if(hubUrl != null) {
                this.setupIERemoteDriver(hubUrl, platform);
            } else {
                this.setupIEDriver();
            }

        } else if(browser.equalsIgnoreCase("chrome")) {
            if(hubUrl != null) {
                this.setupChromeRemoteDriver(hubUrl, platform);
            } else {
                this.setupChromeDriver();
            }

        } else {
            if(hubUrl != null) {
                this.setupFirefoxRemoteDriver(hubUrl, platform);
            } else {

                this.setupFirefoxDriver();
            }
        }

        Pages.setupPages();
        Actions.setupActions();

    }

    @AfterClass
    public void tearDown() {
        Reporter.log(" * Stopping WebDriver");
        this.stopDriver();
    }

}
