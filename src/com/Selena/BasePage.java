/**
 * This is the Base Page class from which every page class is inherited.
 * @author Gergely Brautigam
 */

package com.Selena;

import com.Selena.uielements.LocatorFactory;
import com.Selena.uielements.Page;
import com.Selena.uielements.SelenaLocatorFactory;
import com.Selena.uielements.UISerializer;
import com.Selena.utilities.Reporter;
import com.Selena.utilities.config.SelenaConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Base Page for all pages.
 * 
 * @author Brautigam Gergely
 * 
 */
public class BasePage {


	public static WebDriver driver;
	protected Page elements;
	protected int defaultElementTimeOut = 30;

    public BasePage(final String configFile) {
        this.driver = BaseTest.driver;
        Configuration config = new SelenaConfiguration();
        LocatorFactory locatorFactory = new SelenaLocatorFactory();
        UISerializer uiSerializer = new UISerializer(config, locatorFactory);
        elements = uiSerializer.deserialize(config.getValue(ConfigParams.UIXMLSDIR)
                + File.separator + configFile);
    }


	/**
	 * Fetch a locator from the Page config and assert that it is not null.
	 * 
	 * @param elementName
	 *            The element to fetch
	 * 
	 * @return The element locator
	 * @throws java.io.IOException
	 * @throws java.io.IOException
	 */
	public String getElementLocator(final String elementName) {
		String elementLocator = elements.getElementLocator(elementName);
		Assert.assertNotNull(elementLocator, elementName + " element not found in UIELEMENTS config file");

		return elementLocator;

	}

    public String getPageTitle(String message) {
        Reporter.log(message);
        return driver.getTitle();
    }

    public WebElement getElement(String locatorName) {
        String elementLocator = this.getElementLocator(locatorName);
        return driver.findElement(By.xpath(elementLocator));
    }

    public void selectDropDownListOption(String message, String selectLocator, String selectValue) throws IOException {
        Reporter.log(message);
        String selectXPath = this.getElementLocator(selectLocator);
        Select dropDownList = new Select(driver.findElement(By.xpath(selectXPath)));
        dropDownList.selectByValue(selectValue);
    }

    public void selectDropDownListOptionByXpath(String selectXPath, String selectValue) throws IOException {
        Select dropDownList = new Select(driver.findElement(By.xpath(selectXPath)));
        dropDownList.selectByValue(selectValue);
    }



    public void selectDropDownListOptionByText(String message, String selectLocator, String selectValueText) throws IOException {
        Reporter.log(message);
        String selectXPath = this.getElementLocator(selectLocator);
        Select dropDownList = new Select(driver.findElement(By.xpath(selectXPath)));
        List<WebElement> options = dropDownList.getOptions();
        String optionValue = "UNKNOWN";
        for(WebElement option: options) {
            String title = option.getText();
            if(title.equals(selectValueText)) {
                optionValue = option.getAttribute("value");
            }
        }
        dropDownList.selectByValue(optionValue);
    }

	public void type(String message, String locatorName, String value) throws IOException {
		String elementLocator = this.getElementLocator(locatorName);
        this.typeByXpath(message, elementLocator, value);

	}

    public void typeByXpath(String message, String locatorXpath, String value) throws IOException {
        Reporter.log(message);
        WebElement element = driver.findElement(By.xpath(locatorXpath));
        element.clear();
        element.sendKeys(value);
    }

    public void typeByXpath(String locatorXpath, String value) throws IOException {
        WebElement element = driver.findElement(By.xpath(locatorXpath));
        element.clear();
        element.sendKeys(value);
    }

    public String getTextFromInput(String message, String locatorName) throws IOException {
        return this.getElementAttributeValue(message, locatorName, "value");
    }
	
	public void click(String message, String locatorName) {
		String elementLocator = this.getElementLocator(locatorName);
        this.clickByXpath(message, elementLocator);
	}

    public void clickByXpath(String message, String locatorXpath) {
        Reporter.log(message);
        driver.findElement(By.xpath(locatorXpath)).click();
    }

    public void clickByXpath(String locatorXpath) throws AWTException {
        driver.findElement(By.xpath(locatorXpath)).click();
    }

    public void clickWithJS(String message, String locatorName) {
        String elementLocator = this.getElementLocator(locatorName);
        this.clickByXpathWithJS(message, elementLocator);
    }

    public void clickByXpathWithJS(String message, String locatorXpath) {
        Reporter.log(message);
        ((JavascriptExecutor) driver).executeScript("document.evaluate(\"" + locatorXpath + "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();");
    }
	

	public void clickAndWaitElementVisibility(String message, String locatorName, String locatorNameWaitFor) {
		Reporter.log(message);
		String elementLocator = this.getElementLocator(locatorName);
		driver.findElement(By.xpath(elementLocator)).click();
		this.waitForElementVisibility(locatorNameWaitFor);
		
	}
	
	public void clickAndWaitElementInvisibility(String message, String locatorName, String locatorNameWaitFor) {
		Reporter.log(message);
		String elementLocator = this.getElementLocator(locatorName);
		driver.findElement(By.xpath(elementLocator)).click();
		this.waitForElementInvisibility(locatorNameWaitFor);
	}

    public boolean isElementPresent(String message, String locatorName) {
        int elementsCount = this.getElementsCount(message, locatorName);
        boolean present = false;
        if(elementsCount > 0)
            present = true;
        return present;
    }

    public boolean isElementPresent(String locatorName) {
        int elementsCount = this.getElementsCount(locatorName);
        boolean present = false;
        if(elementsCount > 0)
            present = true;
        return present;
    }

    public boolean isElementPresentByXpath(String message, String locatorXpath) {
        int elementsCount = this.getElementsCountByXpath(message, locatorXpath);
        boolean present = false;
        if(elementsCount > 0)
            present = true;
        return present;
    }

    public boolean isElementPresentByXpath(String locatorXpath) {
        int elementsCount = this.getElementsCountByXpath(locatorXpath);
        boolean present = false;
        if(elementsCount > 0)
            present = true;
        return present;
    }



    public int getElementsCount(String message, String locatorName) {
        String element = this.getElementLocator(locatorName);
        return this.getElementsCountByXpath(message, element);
    }

    public int getElementsCount(String locatorName) {
        String element = this.getElementLocator(locatorName);
        return this.getElementsCountByXpath(element);
    }

    public int getElementsCountByXpath(String message, String xpath) {
        Reporter.log(message);
        return this.getElementsCountByXpath(xpath);
    }

    public int getElementsCountByXpath(String xpath) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        int size =  driver.findElements(By.xpath(xpath)).size();
        driver.manage().timeouts().implicitlyWait(this.defaultElementTimeOut, TimeUnit.SECONDS);
        return size;
    }
	
	public String getElementAttributeValue(String message, String locatorName, String attributeName) {
		Reporter.log(message);
		String element = this.getElementLocator(locatorName);
		return driver.findElement(By.xpath(element)).getAttribute(attributeName);
	}

    public String getElementText(String message, String locatorName) {
        Reporter.log(message);
        return this.getElement(locatorName).getText();
    }

			
	public void waitForElement(String locatorNameWaitFor) {
		String elementLocatorWaitFor = this.getElementLocator(locatorNameWaitFor);
		driver.findElement(By.xpath(elementLocatorWaitFor));
	}

    public void waitForElementByXpath(String locatorXpath) {
        driver.findElement(By.xpath(locatorXpath));
    }
	
	public void waitForElementVisibility(String locatorNameWaitFor) {
		String elementLocatorWaitFor = this.getElementLocator(locatorNameWaitFor);
        this.waitForElementVisibilityByXpath(elementLocatorWaitFor);
	}

    public void waitForElementVisibilityByXpath(String locatorXpath) {
        (new WebDriverWait(driver, defaultElementTimeOut)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locatorXpath)));
    }
	
	public void waitForElementInvisibility(String locatorNameWaitFor) {
        if(this.isElementPresent(locatorNameWaitFor)) {
            String elementLocatorWaitFor = this.getElementLocator(locatorNameWaitFor);
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		    (new WebDriverWait(driver, defaultElementTimeOut)).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(elementLocatorWaitFor)));
            driver.manage().timeouts().implicitlyWait(this.defaultElementTimeOut, TimeUnit.SECONDS);
        }
	}


    public void swithToFrame(String message, String locatorName) {
        Reporter.log(message);
        WebElement frame = this.getElement(locatorName);
        driver.switchTo().frame(frame);
    }
}
