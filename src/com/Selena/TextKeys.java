package com.Selena;

import java.io.IOException;

/**
 * 
 * @author iryna
 */
public interface TextKeys {

	String getTextLocator(String key) throws IOException;

}
