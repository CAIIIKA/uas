/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.Selena;

/**
 * Config Params.
 * @author meza
 */
public final class ConfigParams {

    /**
     * Configuration Directory.
     */
    public static final String CONFIGDIR = "config.dir";

    /**
     * Uixml Dir.
     */
    public static final String UIXMLSDIR = "uixmls.dir";


    /**
     * Selenium Base URL.
     */
//    public static final String SELENIUMBASEURL = "selenium.baseUrl";

    /**
     * Private Constructor.
     */
    private ConfigParams()
    {

    }
}
