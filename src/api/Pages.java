package api;

import api.pages.PostmanPage;

import java.awt.*;

/**
 * @author William
 *
 */
public class Pages {
    private PostmanPage mainPage;

    private static Pages pages;

    private Pages() throws AWTException{
        this.mainPage = new PostmanPage();
    }

    public static void setupPages() throws AWTException{
        pages = new Pages();
    }

    public static PostmanPage mainPage() {
        return pages.mainPage;
    }

}
