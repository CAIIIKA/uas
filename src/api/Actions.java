package api;

import api.actions.GeneralActions;

/**
 * @author William
 *
 */
public class Actions {
    private GeneralActions generalActions;

    private static Actions actions;

    private Actions() {
        this.generalActions = new GeneralActions();
    }

    public static void setupActions() {
        actions = new Actions();
    }

    public static GeneralActions generalActions() {
        return actions.generalActions;
    }

}
