package api.actions;

import com.Selena.BaseActionList;
import api.Pages;

import java.awt.*;
import java.io.IOException;

/**
 * @author William
 *
 */
public class GeneralActions extends BaseActionList {

	public GeneralActions(){
		super();
	}

    public void openMainPage() throws AWTException {
        Pages.mainPage().openPage();
    }

    public void clickSendButton() throws InterruptedException, AWTException {
        Pages.mainPage().sendData();
    }

    public void typeUrl(String url) throws IOException, InterruptedException {
        Pages.mainPage().typeUrl(url);
    }

    public void typeData(String data) throws IOException, AWTException, InterruptedException {
        Pages.mainPage().typeData(data);
    }

    public String getId() throws IOException, InterruptedException {
        return Pages.mainPage().getId();
    }

    public String getStatus() throws IOException, InterruptedException{
        return Pages.mainPage().getStatus();
    }

    public void selectMethod(String method) throws IOException, InterruptedException{
        Pages.mainPage().selectMethod(method);
    }

    public String getToken() throws IOException, InterruptedException{
        return Pages.mainPage().getToken();
    }
}
