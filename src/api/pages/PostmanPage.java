package api.pages;

import com.Selena.BasePage;
import com.Selena.utilities.Reporter;
import org.openqa.selenium.By;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * @author William
 */
public class PostmanPage extends BasePage {
    static Robot robot;

    public PostmanPage() throws AWTException {
        super("PostmanPage.xml");

    }

    public void openPage() throws AWTException{
        Reporter.log("Opening main page");
        driver.get("chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm/index.html");
        Robot robot = new Robot();
        robot.mouseMove(300, 10);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void typeUrl(String url) throws IOException, InterruptedException {
        Reporter.log("Typing url");
        String urlField = super.getElementLocator("urlField");
        super.typeByXpath(urlField, url);
    }

    public void sendData() throws AWTException, InterruptedException {
        Reporter.log("Click send button");
        String sendDataButton = super.getElementLocator("sendButton");
        super.clickByXpath(sendDataButton);
        Robot robot = new Robot();
        Thread.sleep(3000);
        robot.keyPress(KeyEvent.VK_ENTER);
        Thread.sleep(5000);

    }

    public void typeData(String data) throws IOException, AWTException, InterruptedException {
        Reporter.log("Typing data");
        String dataField = super.getElementLocator("dataField");

        robot = new Robot();

        Thread.sleep(5000);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(5000);
        super.clickByXpath(dataField);
        super.clickByXpath(dataField);
        super.clickByXpath(dataField);

        Thread.sleep(5000);
        for (int i = 1; i < 1000; i++)
            robot.keyPress(KeyEvent.VK_BACK_SPACE);
            robot.keyRelease(KeyEvent.VK_BACK_SPACE);

        for (int i = 0; i < data.length(); i++) {
            type(data.charAt(i));
        }
        Thread.sleep(3000);
    }

    public static void type(char c) {
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_NUMPAD0);
        robot.keyRelease(KeyEvent.VK_NUMPAD0);

        String altCode = Integer.toString(c);
        for (int i = 0; i < altCode.length(); i++) {
            c = (char) (altCode.charAt(i) + '0');
            robot.keyPress(c);
            robot.keyRelease(c);
        }
        robot.keyRelease(KeyEvent.VK_ALT);
    }

    public String getId() throws IOException, InterruptedException {
        Reporter.log("Getting ID");
        Thread.sleep(7000);
        String success = super.getElementLocator("idField");
        return driver.findElement(By.xpath(success)).getText();

    }

    public String getToken() throws IOException, InterruptedException {
        Reporter.log("Getting Token");
        Thread.sleep(7000);
        String success = super.getElementLocator("tokenField");
        return driver.findElement(By.xpath(success)).getText();

    }

    public String getStatus() throws IOException, InterruptedException{
        Thread.sleep(5000);
        Reporter.log("Checking success");
        String status = super.getElementLocator("status");
        return driver.findElement(By.xpath(status)).getText();
    }

    public void selectMethod(String method) throws IOException, InterruptedException {
        Reporter.log("Selecting method");
        String methodField = super.getElementLocator("methodField");
        super.selectDropDownListOptionByXpath(methodField, method);
        Thread.sleep(300);
    }
}


