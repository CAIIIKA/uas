package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

/**
 * @author William
 */
public class GetUserByTokenIncorrect extends BaseTest {


    @Test
    public void getUserByTokenIncorrectTest() throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        String token = "sddfsdfsdf";


        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/token");
        Actions.generalActions().typeData("{\n" +
                "\"token\": "+token+"\n" +
                "}\n");

        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("400"), "Status not 400");

    }

}
