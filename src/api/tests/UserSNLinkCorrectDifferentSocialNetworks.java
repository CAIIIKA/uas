package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author William
 */
public class UserSNLinkCorrectDifferentSocialNetworks extends BaseTest {


    @Test
    public void userSNLinkCorrectDifferentSocialNetworkTest() throws IOException, InterruptedException, AWTException, InterruptedException {
        Actions.generalActions().openMainPage();
        String id = "";
        File idFile = new File(System.getProperties().getProperty("output")+ File.separatorChar +"userId.txt");
        id = FileUtils.readFileToString(idFile);
        id = id.replace("\"", "");
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/siebel/"+id+"/snlink");
        Actions.generalActions().typeData("{\n" +
                "\"socialNetwork\": \"Facebook\",\n" +
                "\"socialNetworkKey\": \"100006" + 507370 + (int) (Math.random() * ((907370 - 507370) + 1)) + "\"\n" +
                "}");

        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();

        Assert.assertTrue(Actions.generalActions().getStatus().contains("200"), "Status not 200");

        Actions.generalActions().typeData("{\n" +
                "\"socialNetwork\": \"VKontakte\",\n" +
                "\"socialNetworkKey\": \"100006" + 507370 + (int) (Math.random() * ((907370 - 507370) + 1)) + "\"\n" +
                "}");
        Actions.generalActions().clickSendButton();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("200"), "Status not 200");
    }

}
