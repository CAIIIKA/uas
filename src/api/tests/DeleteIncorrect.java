package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author William
 */
public class DeleteIncorrect extends BaseTest {

    @DataProvider(name = "loginPassword")
    public Object[][] getData() throws Exception {
        Object[][] retObjArr = super.getTableArray(System.getProperties().getProperty("config.dir")	+ File.separatorChar + "data"
                + File.separatorChar + "DataPool.xls", "LoginData", "data1");
        return (retObjArr);
    }

    @Test(dataProvider = "loginPassword")
    public void deleteUserIncorrectTest(String login, String password) throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();

        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/siebel/sdfsdfsdf");
        Actions.generalActions().typeData("");
        Actions.generalActions().selectMethod("DELETE");
        Actions.generalActions().clickSendButton();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("400"), "Status not 400");




    }

}
