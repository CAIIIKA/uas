package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author William
 */
public class GetUserByTokenCorrect extends BaseTest {


    @Test
    public void getUserByTokenTest() throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        String token = "";
        File tokenFile = new File(System.getProperties().getProperty("output")+ File.separatorChar +"token.txt");
        token = FileUtils.readFileToString(tokenFile);

        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/token");
        Actions.generalActions().typeData("{\n" +
                "\"token\": "+token+"\n" +
                "}\n");

        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("200"), "Status not 200");

    }

}
