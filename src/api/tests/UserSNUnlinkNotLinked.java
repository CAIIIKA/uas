package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

/**
 * @author William
 */
public class UserSNUnlinkNotLinked extends BaseTest {


    @Test
    public void userSNUnlinkNotLinkedTest() throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user");
        Actions.generalActions().typeData("{\n" +
                "\"Country\": \"������345444\",\n" +
                "\"FirstName\": \"���675435444\",\n" +
                "\"LastName\": \"������53345t444\",\n" +
                "\"Login\": \"login"+507370 + (int)(Math.random() * ((907370 - 507370) + 1))+"\",\n" +
                "\"Password\": \"password"+507370 + (int)(Math.random() * ((907370 - 507370) + 1))+"\",\n" +
                "\"PatronymicName\": \"������3y4t3445\",\n" +
                "\"Phone\": \"+7465123y5t3454\"\n" +
                "}\n");
        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();
        String id = Actions.generalActions().getId();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("200"), "Status not 200");

        id = id.replace("\"", "");
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/siebel/"+id+"/snunlink");
        Actions.generalActions().typeData("{\n" +
                "\"socialNetwork\": \"Facebook\"\n" +
                "}\n");

        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("400"), "Status not 400");

    }

}
