package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.IOException;

/**
 * @author William
 */
public class CreateUserIncorrect extends BaseTest {


    @Test
    public void createUserIncorrecTest() throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user");
        Actions.generalActions().typeData("{\n" +
//                "\"Country\": \"������\",\n" +
                "\"FirstName\": \"�����������������������������������������������������������������������������������������������������������������������������������������\",\n" +
                "\"LastName\": \"������\",\n" +
                "\"Login\": \"Ivan_Petrov@test.test\",\n" +
                "\"Password\": \"Xhr8e@hg\",\n" +
                "\"PatronymicName\": \"��������\",\n" +
                "\"Phone\": \"+74651234567\"\n" +
                "}\n");
        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();

        Assert.assertTrue(Actions.generalActions().getStatus().contains("400"), "Status not 400");

    }

}
