package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @author William
 */
public class CreateUserCorrect extends BaseTest {

    @DataProvider(name = "loginPassword")
    public Object[][] getData() throws Exception {
        Object[][] retObjArr = super.getTableArray(System.getProperties().getProperty("config.dir")	+ File.separatorChar + "data"
                + File.separatorChar + "DataPool.xls", "LoginData", "data1");
        return (retObjArr);
    }

    @Test(dataProvider = "loginPassword")
    public void createUserTest(String login, String password) throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user");
        Random rnd = new Random();
        login = login + rnd.nextInt();

        File userLogin = new File(System.getProperties().getProperty("output")+ File.separatorChar +"login.txt");

        FileUtils.writeStringToFile(userLogin, login);

        Actions.generalActions().typeData("{\n" +
//                "\"Country\": \"Russia\",\n" +
                "\"FirstName\": \"Ivan\",\n" +
                "\"LastName\": \"Petrov\",\n" +
                "\"Login\": \""+login+"\",\n" +
                "\"Password\": \""+password+"\",\n" +
                "\"PatronymicName\": \"Petrovich\",\n" +
                "\"Phone\": \"+74657654321\"\n" +
                "}\n");
        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();
        System.out.println(Actions.generalActions().getId());
        Assert.assertTrue(Actions.generalActions().getStatus().contains("200"), "Status not 200");

        File userId = new File(System.getProperties().getProperty("output")+ File.separatorChar +"userId.txt");

        FileUtils.writeStringToFile(userId, Actions.generalActions().getId());



    }

}
