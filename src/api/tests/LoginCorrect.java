package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author William
 */
public class LoginCorrect extends BaseTest {


    @DataProvider(name = "loginPassword")
    public Object[][] getData() throws Exception {
        Object[][] retObjArr = super.getTableArray(System.getProperties().getProperty("config.dir")	+ File.separatorChar + "data"
                + File.separatorChar + "DataPool.xls", "LoginData", "data1");
        return (retObjArr);
    }

    @Test(dataProvider = "loginPassword")
    public void loginCorrectTest(String login, String password) throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        String url = "";
        File idFile = new File(System.getProperties().getProperty("output")+ File.separatorChar +"userId.txt");
        url = FileUtils.readFileToString(idFile);
        url = url.replace("\"", "");
        File loginFile = new File(System.getProperties().getProperty("output")+ File.separatorChar +"login.txt");
        login = FileUtils.readFileToString(loginFile);
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/login");
        Actions.generalActions().typeData("{\n" +
                "\"login\": \""+login+"\",\n" +
                "\"password\": \""+password+"\"\n" +
                "}\n");

        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();


        File userId = new File(System.getProperties().getProperty("output")+ File.separatorChar +"token.txt");

        FileUtils.writeStringToFile(userId, Actions.generalActions().getToken());
        Assert.assertTrue(Actions.generalActions().getStatus().contains("200"), "Status not 200");

    }

}
