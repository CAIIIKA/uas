package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author William
 */
public class UpdateUserIncorrect extends BaseTest {


    @Test
    public void updateUserIncorrectTest() throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        String url = "";
        File idFile = new File(System.getProperties().getProperty("output")+ File.separatorChar +"userId.txt");
        url = FileUtils.readFileToString(idFile);
        url = url.replace("\"", "");
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/siebel/" + url+"1");
        Actions.generalActions().typeData("{\n" +
                "\"FirstName\": \"Ivan\",\n" +
                "\"LastName\": \"Ivanov\"\n" +
                "}\n" +
                "\n");

        Actions.generalActions().selectMethod("PUT");
        Actions.generalActions().clickSendButton();

        Assert.assertTrue(Actions.generalActions().getStatus().contains("400"), "Status not 400");

    }

}
