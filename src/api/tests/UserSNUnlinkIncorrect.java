package api.tests;

import api.Actions;
import com.Selena.BaseTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * @author William
 */
public class UserSNUnlinkIncorrect extends BaseTest {


    @Test
    public void userSNUnlinkIncorrectTest() throws IOException, InterruptedException, AWTException {
        Actions.generalActions().openMainPage();
        String id = "";
        File idFile = new File(System.getProperties().getProperty("output")+ File.separatorChar +"userId.txt");
        id = FileUtils.readFileToString(idFile);
        id = id.replace("\"", "");
        Actions.generalActions().typeUrl("https://10.1.232.45:443/authentication/user/siebel/"+id+"/snunlink");
        Actions.generalActions().typeData("{\n" +
                "\"socialNetwork\": \"Twitter\"\n" +
                "}\n");

        Actions.generalActions().selectMethod("POST");
        Actions.generalActions().clickSendButton();
        Assert.assertTrue(Actions.generalActions().getStatus().contains("400"), "Status not 400");

    }

}
