package api.utils.lists;

/**
 * @author William
 *
 */
public enum PersonalTitleSelectValues {
    MR("Mr."),
    MRS("Mrs./Ms."),
    PROF("Prof."),
    DR("Dr."),
    _ERROR("ERROR");

    private String value;

    private PersonalTitleSelectValues(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static PersonalTitleSelectValues getParameterByStringName(String parameterName) {
        PersonalTitleSelectValues value = PersonalTitleSelectValues._ERROR;

        if(parameterName.equalsIgnoreCase("MR"))
            value = PersonalTitleSelectValues.MR;
        if(parameterName.equalsIgnoreCase("MRS"))
            value = PersonalTitleSelectValues.MRS;
        if(parameterName.equalsIgnoreCase("PROF"))
            value = PersonalTitleSelectValues.PROF;
        if(parameterName.equalsIgnoreCase("DR"))
            value = PersonalTitleSelectValues.DR;

        return value;
    }
}
