package api.utils.lists;

/**
 * @author William
 *
 */
public enum CountrySelectValues {
    SWITZERLAND("62"),
    _ERROR("ERROR");

    private String value;

    private CountrySelectValues(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static CountrySelectValues getParameterByStringName(String parameterName) {
        CountrySelectValues value = CountrySelectValues._ERROR;

        if(parameterName.equalsIgnoreCase("SWITZERLAND"))
            value = CountrySelectValues.SWITZERLAND;

        return value;
    }
}
