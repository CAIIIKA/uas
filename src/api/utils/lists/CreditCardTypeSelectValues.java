package api.utils.lists;

/**
 * @author William
 *
 */
public enum CreditCardTypeSelectValues {
    MASTERCARD("6"),
    VISA("7"),
    AMERICAN_EXPRESS("1"),
    DINERS_CLUB("3"),
    _ERROR("ERROR");

    private String value;

    private CreditCardTypeSelectValues(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static CreditCardTypeSelectValues getParameterByStringName(String parameterName) {
        CreditCardTypeSelectValues value = CreditCardTypeSelectValues._ERROR;

        if(parameterName.equalsIgnoreCase("MASTERCARD"))
            value = CreditCardTypeSelectValues.MASTERCARD;
        if(parameterName.equalsIgnoreCase("VISA"))
            value = CreditCardTypeSelectValues.VISA;
        if(parameterName.equalsIgnoreCase("AMERICAN_EXPRESS"))
            value = CreditCardTypeSelectValues.AMERICAN_EXPRESS;
        if(parameterName.equalsIgnoreCase("DINERS_CLUB"))
            value = CreditCardTypeSelectValues.DINERS_CLUB;

        return value;
    }
}
