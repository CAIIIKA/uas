package api.utils;

/**
 * @author William
 *
 */
public class Formatter {

    /**
     * Escape special characters for HTML logs
     * @param string string with unescaped characters
     * @return string with escaped characters
     */
    public static String escapeCharacters(String string) {
        return string.replace("&", "&#38;");
    }
}
