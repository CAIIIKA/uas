<!DOCTYPE html>
<!--[if lt IE 8]> <html lang="en-US" class="no-touch no-js ie7 ie full" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8]> <html lang="en-US" class="no-touch no-js ie8 ie full" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9]> <html lang="en-US" class="no-touch no-js no-ie ie9 full" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" class="no-ie no-touch no-js full"> <!--<![endif]-->

<head>

	<!-- META -->

	<meta charset="UTF-8" />
	<meta name="author" content="rubenbristian.com" />
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<!-- TITLE & SEO -->

	<title>Performance Testing Mobile Applications with Microsoft Visual Studio / Team Foundation Server | Jamo Solutions</title>
	<meta name="description" content="Test mobile and embedded applications on iOS, Android, Windows Mobile/CE, Windows Phone and many others using testing tools such as UFT, QTP, Eclipse and Visual Studio." >
	<meta name="keywords" content="Mobile Application Testing, Mobile Performance Testing, Mobile Functional Testing, Mobile Devices, Phones, Smartphones" >


	<!-- LINKS -->

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="http://www.jamosolutions.com/xmlrpc.php" />
	<link rel="shortcut icon" type="image/x-icon" href="http://www.jamosolutions.com/wp-content/uploads/2013/07/jamo_favicon.png" />

	<!-- STYLES -->

	<!--<link rel="stylesheet" type="text/css" media="all" href="http://www.jamosolutions.com/wp-content/themes/goodwork/style.css" />-->

	
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<!-- WP HEAD -->

	<link rel="alternate" type="application/rss+xml" title="Jamo Solutions &raquo; Feed" href="http://www.jamosolutions.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Jamo Solutions &raquo; Comments Feed" href="http://www.jamosolutions.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Jamo Solutions &raquo; Performance Testing Mobile Applications with Microsoft Visual Studio / Team Foundation Server Comments Feed" href="http://www.jamosolutions.com/performance-testing-with-microsoft-visual-studio-team-foundation-server/feed/" />
<link rel='stylesheet' id='cnss_css-css'  href='http://www.jamosolutions.com/wp-content/plugins/easy-social-icons/css/cnss.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='prettyphoto-css'  href='http://www.jamosolutions.com/wp-content/plugins/prettyphoto-media/css/prettyPhoto.css?ver=3.1.4' type='text/css' media='screen' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://www.jamosolutions.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='wp-members-css'  href='http://www.jamosolutions.com/wp-content/themes/goodwork/style.css?ver=3.7.1' type='text/css' media='all' />
<script type='text/javascript' src='http://www.jamosolutions.com/wp-includes/js/jquery/jquery.js?ver=1.10.2'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-content/plugins/easy-social-icons/js/cnss.js?ver=1.0'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.jamosolutions.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.jamosolutions.com/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Performance Testing Mobile Applications with HP LoadRunner' href='http://www.jamosolutions.com/product/performance-testing-and-monitoring-of-mobile-applications/performance-testing-mobile-applications-with-hp-loadrunner/' />
<link rel='next' title='Windows Phone 8/ Windows 8 Test Automation using Unified Functional Testing' href='http://www.jamosolutions.com/product/test-automation/windows-phone-8-windows-8-test-automation/winphone-testing-automation-using-qtp/' />
<meta name="generator" content="WordPress 3.7.1" />
<link rel='canonical' href='http://www.jamosolutions.com/performance-testing-with-microsoft-visual-studio-team-foundation-server/' />
<link rel='shortlink' href='http://www.jamosolutions.com/?p=2204' />
<!-- WP-Members version 2.8.6, available at http://rocketgeek.com/wp-members -->

		<link id="f_head" href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		
		<link id="f_body" href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		
		<link id="f_quote" href='http://fonts.googleapis.com/css?family=Droid+Serif:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		
		<style id="fonts-bg" type="text/css">

			/* CUSTOM FONTS */

			h1, h2, h3, h4, h5, h6, .tp-caption {
			  font-family:'Open Sans Condensed', sans-serif;
			}

			body, .rbStats.pie h5, .rbStats.bars h5, .rbTwitter > a h5, .fancybox-title, .tp-caption > a, button, .woocommerce .buttons a {
			  font-family:'Open Sans', sans-serif;
			}

			.modern blockquote, .post blockquote, .rbTestimonial blockquote {
			  font-family:'Droid Serif', serif;
			}

			/* CUSTOM BACKGROUND */
			
			html {
				background-color:#F7F7F7 !important;
			}

			body {
				background:rgba(255,255,255,1);
			}

		</style>

		<style id="colors" type="text/css">

			/* CUSTOM COLORS */

			i.i-medium,
			input[type="submit"]:hover,
			.no-touch .buttons a:hover,
			.no-touch .modern .post:not(.opened) > a:hover .pTitle:before,
			.no-touch .classic .pTitle:hover h2:before,
			.no-touch #comments-title:hover:after,
			.no-touch .rbAccordion.large h4:hover:before,
			.no-touch .rbPosts.classic header:hover a:before,
			.rbPricingTable .featured header,
			.no-touch .sectionTitle h3:hover i,
			.no-touch .rbTextIcon.large > a:hover > i,
			.no-touch .rbSocial.thumbnails ul li:hover,
			.no-touch .tagcloud > a:hover,
			.no-touch #footer1 input[type="submit"]:hover,
			.no-touch #footer1 .tagcloud > a:hover,
			.minimal-2 .tabs span,
			.no-touch .dark_menu #menu li ul li:hover > a, .no-touch #menu li > ul li:hover > a,
			.no-touch .complex .side .nav a:hover,
			.mejs-controls .mejs-time-rail .mejs-time-current,
			.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current,
			.mejs-controls .mejs-volume-button .mejs-volume-slider .mejs-volume-current,
			.no-touch #searchform .holder:hover .icon-search,
			.no-touch .rbButton.dark:hover, .rbButton.light, .no-touch input[type="submit"]:hover,
			.product .button.add_to_cart, .cart-contents:hover:before, .widget_price_filter .price_slider_amount .button:hover, .woocommerce .buttons a:hover, .woocommerce-pagination li a:hover, .product_list_widget .imgCover, .single-product .cart .button:hover, .product-quantity .button:hover, .single-product .cart input:hover, .product-quantity input:hover, .shop_table .product-remove a:hover, .checkout-button.button, .woocommerce button.button:hover {
				background-color:#9c2719;
			}
			#pageTitle h1,
						.no-touch a:hover, .no-touch #menu a:hover,
			.no-touch #pageTitle a:hover,
			.no-touch.csstransitions .tone #items a:hover h3, .tone #items a.iehover h3,
			.no-touch .modern .post:not(.opened) > a:hover .pTitle,
			.no-touch .morePosts:not(.nomore):hover span,
			.no-touch .morePosts:not(.nomore):hover span:before,
			.no-touch .classic .pTitle h2:hover,
			.no-touch .classic .meta a:hover,
			.no-touch #comments-title:hover,
			.no-touch #comments-title:hover:before,
			.no-touch .comment-reply-link:hover:before,
			.await,
			#comment-status p,
			.no-touch .rbAccordion.small > section h4:hover, .no-touch .rbAccordion.small > section h4:hover:before,
			.no-touch .rbAccordion.large h4:hover,
			.no-touch .rbContactInfo ul li a:hover:before,
			.no-touch .rbContactInfo ul li a:hover,
			.errorMessage,
			.no-touch .rbCustomPosts a:hover h4,
			.no-touch .rbCustomPosts a:hover .comments i:before,
			.no-touch .rbPosts.classic header:hover h3,
			.no-touch .sectionTitle h3:hover,
			.no-touch .rbTextIcon > a:hover > h4,
			.no-touch .rbTextIcon.minimal > a:hover > i,
			.no-touch .list1:hover, .no-touch .list2:hover, .no-touch .list3:hover, .no-touch .list4:hover,
			.no-touch .rbTwitter li a:hover, .no-touch .rbTwitter .time:hover, .no-touch .rbTwitter > a:hover span,
			.no-touch .rbSocial.list li:hover:before, .no-touch .rbSocial.list li:hover a,
			.no-touch .rbSocial.icons li:hover:before,
			.no-touch .rbTabs .titles li a:hover,
			.no-touch .widget.email a:hover i, .no-touch .widget.phone a:hover i,
			.no-touch .widget_meta ul li a:hover:before, .no-touch .widget_meta ul li a:hover,
			.no-touch .widget_pages ul li a:hover, .no-touch .widget_categories ul li a:hover, .no-touch .widget_archive ul li a:hover, .no-touch .widget_recent_entries ul li a:hover, .no-touch .widget_recent_comments ul li a:hover, .no-touch .widget_rss ul li a:hover, #breadcrumb a:hover, #breadcrumb .icon-home:hover, .no-touch #menu > ul > li:hover > a, .no-touch .rbButton.light:hover, .woocommerce-pagination .next:hover, .woocommerce-pagination .prev:hover, p.out-of-stock {
				color:#9c2719;
			}

			.ttwo #items .caption,
			.no-touch .anything.folio .arrow a:hover span,
			.minimal-1 .mainControls .m-timer,
			.no-touch .minimal-1 .mainControls .thumbNav li a:hover,
			.no-touch .minimal-1 .mainControls .arrow a:hover span,
			.no-touch .mejs-overlay:hover .mejs-overlay-button, .ch .hover, 
			.no-touch .fancybox-nav span:hover, .no-touch .fancybox-close:hover, .ch .hover, .onsale, .tp-bannertimer {
				background-color:#9c2719;
				background-color:rgba(156,39,25,0.9);
			}

			.no-touch #menu > ul > li:hover > a,
			.no-touch.csstransitions .tone #items a:hover .caption, .tone #items a.iehover .caption,
			.no-touch .morePosts:not(.nomore):hover,
			.no-touch .morePosts:not(.nomore):hover span,
			.no-touch #comments-title:hover:after,
			.contactErrorBorder,
			.no-touch .sectionTitle h3:hover, .no-touch .rbTextIcon.large:hover, .rbTabs .titles li.opened a, .widget_nav_menu .current-menu-item, .ui-slider-horizontal .ui-slider-handle:hover {
				border-color:#9c2719;
			}

			.no-touch .blank .tparrows:hover:before {
			  background:#9c2719;
			}
			
			@media all and (max-width: 940px) {

				.no-touch #menu .responsive:hover {
					background:#9c2719;
				}
				#menu li a:hover {
					color:#9c2719 !important;
				}

			}

			/* CUSTOM CSS */

			/\* there are no custom styes \*/
		</style>
	   	
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic' rel='stylesheet' type='text/css'>
<script type="text/javascript">
	//Sidebar Menu
jQuery(document).ready(function() {
	jQuery('.widget .menu li').mouseenter(
		function() {
			jQuery(this).find('ul').show(200);
		}
	),
	jQuery('.widget .menu li:not(.current-menu-ancestor, .current-menu-item)').mouseleave(
		function() {
			jQuery(this).find('ul').hide(200);
		}
	);
});
</script>
</head>

<body id="body" class="page page-id-2204 page-template-default left-sidebar performance wpb-js-composer js-comp-ver-4.0.0 vc_responsive">

	<!-- Main Wrapper Start -->

	<div class="wrapper clearfix ttfalse">

		<!-- Header Start -->

		<header id="mainHeader" class="f_width light_menu logo_left">

			<div id="logo" class="clearfix">
				<a href="http://www.jamosolutions.com" style="width:250px">
					<img class="default" src="/wp-content/uploads/2013/07/jamo_logo.jpg" alt="Jamo Solutions" />
					<img class="retina" src="/wp-content/uploads/2013/07/jamo_logo.jpg" alt="Jamo Solutions" />
				</a>
			</div>

			<div id="headerWidgets" class="clearfix">
				<div class="left clearfix">
					<div id="cnss_widget-2" class="widget sidebox widget_cnss_widget"><table class="cnss-social-icon" style="width:160px" border="0" cellspacing="0" cellpadding="0"><tr><td style="width:32px"><a target="_blank" title="Facebook" href="https://www.facebook.com/JamoSolutions"><img src="http://www.jamosolutions.com/wp-content/uploads/1373993539_facebook.png" border="0" width="28" height="28" alt="Facebook" /></a></td><td style="width:32px"><a target="_blank" title="Twitter" href="https://twitter.com/jamosolutions"><img src="http://www.jamosolutions.com/wp-content/uploads/1373993589_twitter.png" border="0" width="28" height="28" alt="Twitter" /></a></td><td style="width:32px"><a target="_blank" title="Linkedin" href="http://www.linkedin.com/company/jamo-solutions"><img src="http://www.jamosolutions.com/wp-content/uploads/1373993615_linkedIn.png" border="0" width="28" height="28" alt="Linkedin" /></a></td><td style="width:32px"><a target="_blank" title="Youtube" href="http://www.youtube.com/user/jamosols"><img src="http://www.jamosolutions.com/wp-content/uploads/1373993651_youtube.png" border="0" width="28" height="28" alt="Youtube" /></a></td><td style="width:32px"><a target="_blank" title="Rss" href="/feed/rss2/"><img src="http://www.jamosolutions.com/wp-content/uploads/1373993696_rss.png" border="0" width="28" height="28" alt="Rss" /></a></td></tr></table></div><div id="text-9" class="widget sidebox widget_text">			<div class="textwidget"><a class="rbButton dark large trial_lisence_btn" href="/trial-license/" target="_self">Trial license</a></div>
		</div>				</div>
				<div class="right clearfix">
									</div>
			</div>

			<nav id="menu" class="cartfalse">
				<p class="responsive">Navigation</p>

				<ul id="menu-main-menu" class="clearfix"><li id="menu-item-21" class="menu-item"><a href="http://www.jamosolutions.com/">Home</a></li>
<li id="menu-item-1734" class="selected parent menu-item"><a href="http://www.jamosolutions.com/product/">Products</a><ul class="clearfix">	<li id="menu-item-1678" class="parent menu-item"><a href="http://www.jamosolutions.com/product/test-automation/">Test Automation</a><ul class="clearfix">		<li id="menu-item-1679" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/ios/">iOS</a></li>
		<li id="menu-item-1680" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/android-test-automation/">Android</a></li>
		<li id="menu-item-1681" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/windows-mobilece-test-automation/">Windows Mobile/CE</a></li>
		<li id="menu-item-1682" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/windows-phone-8-windows-8-test-automation/">Windows Phone</a></li>
		<li id="menu-item-1683" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/blackberry-test-automation/">BlackBerry</a></li>
		<li id="menu-item-1684" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/web-applicationtest-automation/">Web Application</a></li>
		<li id="menu-item-1685" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/sencha-ui-object-driven-test-automation-on-ios-and-android-apps/">Sencha</a></li>
		<li id="menu-item-1686" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/resco-for-net-compact-framework/">Resco</a></li>
		<li id="menu-item-1687" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/uftqtp/">UFT/QTP</a></li>
		<li id="menu-item-1688" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/visual-studio/">Visual Studio</a></li>
		<li id="menu-item-1689" class="menu-item"><a href="http://www.jamosolutions.com/product/test-automation/eclipse/">Eclipse</a></li>
</ul>
</li>
	<li id="menu-item-1690" class="selected parent menu-item"><a href="http://www.jamosolutions.com/product/performance-testing-and-monitoring-of-mobile-applications/">Performance Testing</a><ul class="clearfix">		<li id="menu-item-2218" class="menu-item"><a href="http://www.jamosolutions.com/product/performance-testing-and-monitoring-of-mobile-applications/performance-testing-mobile-applications-with-hp-loadrunner/">LoadRunner</a></li>
		<li id="menu-item-2219" class="selected menu-item"><a href="http://www.jamosolutions.com/performance-testing-with-microsoft-visual-studio-team-foundation-server/">Visual Studio</a></li>
</ul>
</li>
	<li id="menu-item-1691" class="menu-item"><a href="http://www.jamosolutions.com/product/m-eux-test-integration-with-test-management-software/">Test Management</a></li>
	<li id="menu-item-1215" class="menu-item"><a href="http://www.jamosolutions.com/product/supported-technologies/">Supported Technologies</a></li>
	<li id="menu-item-1853" class="menu-item"><a href="http://www.jamosolutions.com/product/technology-comparison/">Technology Comparison</a></li>
	<li id="menu-item-1213" class="menu-item"><a href="http://www.jamosolutions.com/product/licensing/">Licensing</a></li>
</ul>
</li>
<li id="menu-item-55" class="parent menu-item"><a href="http://www.jamosolutions.com/member-area/">Member Area</a><ul class="clearfix">	<li id="menu-item-1912" class="menu-item"><a href="http://www.jamosolutions.com/prospects-login/">Prospects Login</a></li>
	<li id="menu-item-1910" class="menu-item"><a href="http://www.jamosolutions.com/customers-login/">Customers Login</a></li>
	<li id="menu-item-1911" class="menu-item"><a href="http://www.jamosolutions.com/partners-login/">Partners Login</a></li>
</ul>
</li>
<li id="menu-item-84" class="parent menu-item"><a href="http://www.jamosolutions.com/about-jamo/">About</a><ul class="clearfix">	<li id="menu-item-714" class="menu-item"><a href="http://www.jamosolutions.com/about-jamo/">About Jamo</a></li>
	<li id="menu-item-83" class="menu-item"><a href="http://www.jamosolutions.com/customers/">Customers</a></li>
	<li id="menu-item-82" class="menu-item"><a href="http://www.jamosolutions.com/partners/">Partners</a></li>
	<li id="menu-item-81" class="menu-item"><a href="http://www.jamosolutions.com/career/">Career</a></li>
	<li id="menu-item-80" class="menu-item"><a href="http://www.jamosolutions.com/contact-us/">Contact us</a></li>
</ul>
</li>
<li id="menu-item-601" class="menu-item"><a href="http://www.jamosolutions.com/videos/">Videos</a></li>
<li id="menu-item-87" class="menu-item"><a href="http://www.jamosolutions.com/blog/">Blog</a></li>
</ul>
				
	<form role="search" method="get" id="searchform" class="searchBox" action="http://www.jamosolutions.com/" >
		<label class="screen-reader-text hidden" for="s">Search for:</label>
		<input type="search" data-value="Type and hit Enter" value="Type and hit Enter" name="s" id="s" />
		<!--<input type="submit" id="searchsubmit" value="Search" />-->
    </form>
			</nav>

			
		</header>
		
		<!-- Header End -->
		

		<!-- Content Wrapper Start -->

		
		<article id="content" class="clearfix"> 

				<h3>Performance Testing Mobile Applications with Microsoft Visual Studio / Team Foundation Server</h3>		
		<p>M-eux Test integrates seamlessly with Microsoft Visual Studio, allowing you to test the performance of your mobile application. M-eux Test supports a wide range of mobile operating systems, ranging from iOS over Android to Windows Phone, Windows CE and Windows Mobile.</p>
<p>Using M-eux Test, you can script user actions on the mobile device and measure the time it takes the mobile device to complete that action.</p>
<p><a href="http://www.jamosolutions.com/wp-content/uploads/2013/09/MobileLoadPerformanceTestVSOnly.png" rel="prettyPhoto"><img class="alignnone size-medium wp-image-2213" alt="MobileLoadPerformanceTestVSOnly" src="http://www.jamosolutions.com/wp-content/uploads/2013/09/MobileLoadPerformanceTestVSOnly-300x283.png" width="300" height="283" /></a> <a href="http://www.jamosolutions.com/wp-content/uploads/2013/09/MobileUnitTestVSOnly.png" rel="prettyPhoto"><img class="alignnone size-medium wp-image-2211 alignleft" alt="MobileUnitTestVSOnly" src="http://www.jamosolutions.com/wp-content/uploads/2013/09/MobileUnitTestVSOnly-300x282.png" width="300" height="282" /></a></p>
<p>Through the use of physical or virtual devices (or a combination of both), you can:</p>
<ul>
<li>Monitor the performance of your application as perceived by the end user</li>
<li>Generate an accurate load on your backend infrastructure and test its ability to handle the mobile load</li>
</ul>
<p>Through our integration with Microsoft Visual Studio, we also allow you to track the performance of your application over time using Microsoft Team Foundation Server.</p>
<p>For more information on performance testing of mobile applications, please see our whitepaper. For further information on M-eux Test and  its integration with Microsoft Visual Studio, please request a <a title="Trial License" href="http://www.jamosolutions.com/trial-license/">trial license</a>.</p>
<br /> 
<script type="text/javascript">
jQuery(function() {
    var focusables = jQuery("input");   
    focusables.keyup(function(e) {
        var maxlength = false;
        if (jQuery(this).attr("maxlength")) {
            if (jQuery(this).val().length >= jQuery(this).attr("maxlength"))
                maxlength = true;
            }
        if (e.keyCode == 13 || maxlength) {
            var current = focusables.index(this),
                next = focusables.eq(current+1).length ? focusables.eq(current+1) : focusables.eq(0);
                next.focus();
        }
    });
	
	/*jQuery(window).load(function() {
		var mainTitle = jQuery('#pageTitle').text();
		var secTitle = jQuery('#content h3:first-child').text();
		if(jQuery.trim(mainTitle) == jQuery.trim(secTitle)){
			jQuery('#content h3:first-child').hide();
		}
	});*/
});
</script>

		</article>

		<!-- Content Wrapper End -->

		<!-- Sidebars -->

        			<aside id="sidebarLeft" class="sidebar clearfix">
				<section id="nav_menu-17" class="widget sidebox widget_nav_menu clearfix"><div class="menu-performance-testing-side-menu-container"><ul id="menu-performance-testing-side-menu" class="menu"><li id="menu-item-2206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2206"><a href="http://www.jamosolutions.com/product/performance-testing-and-monitoring-of-mobile-applications/performance-testing-mobile-applications-with-hp-loadrunner/">LoadRunner</a></li>
<li id="menu-item-2207" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2204 current_page_item menu-item-2207"><a href="http://www.jamosolutions.com/performance-testing-with-microsoft-visual-studio-team-foundation-server/">Visual Studio</a></li>
</ul></div></section>			</aside>
		
	</div>

	<!-- Main Wrapper End -->

	<!-- Footer #1 Wrapper Start -->

	
	<footer id="footer1" class="clearfix">

		<div class="row-fluid">

			
			<div class="column_container span3">
				<section id="text-3" class="widget widget_text clearfix"><div class="widget_title"><h5>About Jamo</h5></div>			<div class="textwidget"><div style="text-align:justify;">Jamo Solutions provides the solution that allows you to do test automation on mobile devices. Using the Jamo solutions M-eux Test software you will be able to replay the actions of the end user directly on the mobile devices.</div></div>
		</section>			</div>

			<div class="column_container span3 clearfix">
				<section id="text-4" class="widget widget_text clearfix"><div class="widget_title"><h5>Get In Touch</h5></div>			<div class="textwidget"><section class="rbContactInfo  autop">
<ul>
<li class="address">Albertvest 8, 3300 Tienen, Belgium</li>
<li class="phone">+32 (0) 16 78 27 02</li>
<li class="email"><a href="mailto:info@jamosolutions.com">info@jamosolutions.com</a></li>
<li class="email"><a href="mailto:support@jamosolutions.com">support@jamosolutions.com</a></li>
<li class="flag"><a href="http://goo.gl/maps/bZoxW">Find us on the map</a></li>
</ul>
</section></div>
		</section>			</div>

			<div class="column_container span3 clearfix">
				<section id="text-10" class="widget widget_text clearfix"><div class="widget_title"><h5>Affiliated Partners</h5></div>			<div class="textwidget"><p><a href="http://h20195.www2.hp.com/v2/GetPDF.aspx%2F4AA2-7048ENW.pdf" target="_blank">
<img src="/wp-content/uploads/hp-footer.png" style="width: 120px;" />
</a></p>
<p><a href="http://www-304.ibm.com/partnerworld/gsd/solutiondetails.do?solution=49302&expand=true&lc=en" target="_blank">
<img src="/wp-content/uploads/ibm-footer.png" style="width: 120px;" />
</a></p>
<p><a href="http://visualstudiogallery.msdn.microsoft.com/6fe22d05-4396-4f20-9206-c9c86c4ee690" target="_blank">
<img src="/wp-content/uploads/ms-footer.png" style="width: 120px;" />
</a></p></div>
		</section>			</div>

			<div class="column_container span3">
				<section id="text-8" class="widget widget_text clearfix">			<div class="textwidget"><section class="rbTwitter clearfix">
		<a href="https://twitter.com/JamoSolutions"><img src="/wp-content/uploads/jamo_icon.png" alt="Jamo Solutions" /></a>
		<a href="https://twitter.com/JamoSolutions"><h5>Jamo Solutions</h5></a>
		<a href="https://twitter.com/JamoSolutions"><span>@JamoSolutions</span></a>
	    <iframe src="//platform.twitter.com/widgets/follow_button.html?show_screen_name=false&lang=en&show_count=false&screen_name=JamoSolutions" style="width:100px; height:24px;"></iframe>
		<ul><li>
			<p class="body">Reuse of desktop browser scripts on mobile browser. <a href="http://t.co/W4GvxBKp0k">http://t.co/W4GvxBKp0k</a></p>
			<a class="time" href="https://twitter.com/rubenbristian/status/393656613288288256">25 October 2013 at 8:33 AM</a>
			<div class="intents">
				<a class="popup reply" data-name="Post a Tweet on Twitter" data-width="400" data-height="200" href="https://twitter.com/intent/tweet?in_reply_to=393656613288288256">Reply</a>
				<a class="popup retweet" data-name="Retweet" data-width="400" data-height="200" href="https://twitter.com/intent/retweet?tweet_id=393656613288288256">Retweet</a>
				<a class="popup favorite" data-name="Favorite" data-width="400" data-height="200" href="https://twitter.com/intent/favorite?tweet_id=393656613288288256">Favorite</a>
			</div>
		</li><li>
			<p class="body">Is testing for mobile really that different? by <a href="https://twitter.com/@FrederikCarlier">@FrederikCarlier</a> <a href="http://t.co/ngJUQ3br7K">http://t.co/ngJUQ3br7K</a>  CC  <a href="https://twitter.com/@JamoSolutions">@JamoSolutions</a></p>
			<a class="time" href="https://twitter.com/rubenbristian/status/388409636098801664">10 October 2013 at 9:04 PM</a>
			<div class="intents">
				<a class="popup reply" data-name="Post a Tweet on Twitter" data-width="400" data-height="200" href="https://twitter.com/intent/tweet?in_reply_to=388409636098801664">Reply</a>
				<a class="popup retweet" data-name="Retweet" data-width="400" data-height="200" href="https://twitter.com/intent/retweet?tweet_id=388409636098801664">Retweet</a>
				<a class="popup favorite" data-name="Favorite" data-width="400" data-height="200" href="https://twitter.com/intent/favorite?tweet_id=388409636098801664">Favorite</a>
			</div>
		</li><li>
			<p class="body">We had a successful turnout at StarWEST (Anaheim, CA)! <a href="http://t.co/hvBWYizsLk">http://t.co/hvBWYizsLk</a></p>
			<a class="time" href="https://twitter.com/rubenbristian/status/387876267397111808">9 October 2013 at 9:44 AM</a>
			<div class="intents">
				<a class="popup reply" data-name="Post a Tweet on Twitter" data-width="400" data-height="200" href="https://twitter.com/intent/tweet?in_reply_to=387876267397111808">Reply</a>
				<a class="popup retweet" data-name="Retweet" data-width="400" data-height="200" href="https://twitter.com/intent/retweet?tweet_id=387876267397111808">Retweet</a>
				<a class="popup favorite" data-name="Favorite" data-width="400" data-height="200" href="https://twitter.com/intent/favorite?tweet_id=387876267397111808">Favorite</a>
			</div>
		</li><li>
			<p class="body">Check out our new website, with lots of exciting product information and new releases! <a href="http://t.co/kjgivGd4Z5">http://t.co/kjgivGd4Z5</a></p>
			<a class="time" href="https://twitter.com/rubenbristian/status/387214756811001856">7 October 2013 at 1:56 PM</a>
			<div class="intents">
				<a class="popup reply" data-name="Post a Tweet on Twitter" data-width="400" data-height="200" href="https://twitter.com/intent/tweet?in_reply_to=387214756811001856">Reply</a>
				<a class="popup retweet" data-name="Retweet" data-width="400" data-height="200" href="https://twitter.com/intent/retweet?tweet_id=387214756811001856">Retweet</a>
				<a class="popup favorite" data-name="Favorite" data-width="400" data-height="200" href="https://twitter.com/intent/favorite?tweet_id=387214756811001856">Favorite</a>
			</div>
		</li><li>
			<p class="body">Jamo is on the move! Find us in Australia (Sydney, Melbourne), China (Beijing, Guangzhou), USA (CA), India for the next few months!</p>
			<a class="time" href="https://twitter.com/rubenbristian/status/366832663685890048">12 August 2013 at 8:04 AM</a>
			<div class="intents">
				<a class="popup reply" data-name="Post a Tweet on Twitter" data-width="400" data-height="200" href="https://twitter.com/intent/tweet?in_reply_to=366832663685890048">Reply</a>
				<a class="popup retweet" data-name="Retweet" data-width="400" data-height="200" href="https://twitter.com/intent/retweet?tweet_id=366832663685890048">Retweet</a>
				<a class="popup favorite" data-name="Favorite" data-width="400" data-height="200" href="https://twitter.com/intent/favorite?tweet_id=366832663685890048">Favorite</a>
			</div>
		</li></ul></section></div>
		</section>			</div>

			

		</div>

    </footer>

    
	<!-- Footer #1 Wrapper End -->

	<!-- Footer #2 Wrapper Start -->

	<footer id="footer2" class="clearfix">

		<div class="clearfix">

			<div class="clearfix center">
				<div id="text-6" class="widget widget_text">			<div class="textwidget"><p>All rights reserved to Jamo Solutions NV, 2013</p></div>
		</div>			</div>

			<div class="right clearfix">
							</div>

		</div>
		<div class="credits">
			<a href="http://www.inart.co.il" target="_blank">Developed By <img src="/wp-content/themes/goodwork/images/inArt-footer.png" /></a>
		</div>

    </footer>

	<!-- Footer #2 Wrapper End -->

	<div id="scripts">

		<script type="text/javascript"> var _daAnalytics = _daAnalytics || {}; _daAnalytics.init = function (d) {_daAnalytics.Commands = _daAnalytics.Commands || []; for (var a = function (a) { return function () { _daAnalytics.Commands.push([a].concat(Array.prototype.slice.call(arguments, 0))) } }, b = "siteId trackLinkClicks setUserId setProperty setView trackPage trackAction trackEvent trackView setAppId setAccountId setUserId".split(" "), c = 0; c < b.length; c++) _daAnalytics[b[c]] = a(b[c]); _daAnalytics.setAppId(d); var a = document.createElement("script"); a.type = "text/javascript"; a.src = "//az416426.vo.msecnd.net/scripts/da.js"; a.async = !0; var b = document.getElementsByTagName("script")[0]; b.parentNode.insertBefore(a, b); }; _daAnalytics.init("432e370a-8243-4b91-aed4-9de5b8a94c86"); _daAnalytics.trackPage(); 
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43144531-1', 'jamosolutions.com');
  ga('send', 'pageview');
</script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.10.3'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.10.3'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-includes/js/jquery/ui/jquery.ui.mouse.min.js?ver=1.10.3'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-includes/js/jquery/ui/jquery.ui.sortable.min.js?ver=1.10.3'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-content/plugins/prettyphoto-media/js/jquery.prettyPhoto.min.js?ver=3.1.4'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.44.0-2013.09.15'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/www.jamosolutions.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.5.3'></script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-content/themes/goodwork/js/plugins.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var theme_objects = {"base":"http:\/\/www.jamosolutions.com\/wp-content\/themes\/goodwork","colorAccent":"#9c2719","secondColor":"#DBDBDB","blogPage":"8","commentProcess":"Processing your comment...","commentError":"You might have left one of the fields blank, or be posting too quickly.","commentSuccess":"Thanks for your response. Your comment will be published shortly after it'll be moderated."};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.jamosolutions.com/wp-content/themes/goodwork/js/scripts.min.js'></script>
<script>
jQuery(function($) {
$('a[rel^="prettyPhoto"]').prettyPhoto({ show_title: false, deeplinking: false, social_tools: '<div class="twitter"><iframe allowtransparency="true" frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html?count=none&amp;url={location_href}" style="border:none; overflow:hidden; width:59px; height:20px;"></iframe></div><div class="facebook"><iframe src="//www.facebook.com/plugins/like.php?href={location_href}&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:21px;" allowTransparency="true"></iframe></div>' });
});
</script>

	</div>

	<div id="oldie">
		<p>This is a unique website which will require a more modern browser to work!		<a href="https://www.google.com/chrome/" target="_blank">Please upgrade today!</a>
		</p>
	</div>

</body>
</html>